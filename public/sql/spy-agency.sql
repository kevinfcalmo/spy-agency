-- This script was generated by a beta version of the ERD tool in pgAdmin 4.
-- Please log an issue at https://redmine.postgresql.org/projects/pgadmin4/issues/new if you find any bugs, including reproduction steps.
BEGIN;

CREATE DATABASE IF NOT EXISTS public.spy-agency

CREATE TABLE IF NOT EXISTS public.agents
(
    id integer NOT NULL,
    last_name character varying(255) COLLATE pg_catalog."default" NOT NULL,
    first_name character varying(255) COLLATE pg_catalog."default" NOT NULL,
    birth_date date NOT NULL,
    authentication_code character varying(255) COLLATE pg_catalog."default" NOT NULL,
    country_id integer,
    CONSTRAINT agents_pkey PRIMARY KEY (id)
);

CREATE TABLE IF NOT EXISTS public.agents_specialities
(
    agents_id integer NOT NULL,
    specialities_id integer NOT NULL,
    CONSTRAINT agents_specialities_pkey PRIMARY KEY (agents_id, specialities_id)
);

CREATE TABLE IF NOT EXISTS public.contacts
(
    id integer NOT NULL,
    last_name character varying(255) COLLATE pg_catalog."default" NOT NULL,
    first_name character varying(255) COLLATE pg_catalog."default" NOT NULL,
    birth_date date NOT NULL,
    code_name character varying(255) COLLATE pg_catalog."default" NOT NULL,
    country_id integer,
    CONSTRAINT contacts_pkey PRIMARY KEY (id)
);

CREATE TABLE IF NOT EXISTS public.country
(
    id integer NOT NULL,
    name character varying(255) COLLATE pg_catalog."default" NOT NULL,
    iso_code character varying(2) COLLATE pg_catalog."default" NOT NULL,
    isd_code character varying(7) COLLATE pg_catalog."default" NOT NULL,
    CONSTRAINT country_pkey PRIMARY KEY (id)
);

CREATE TABLE IF NOT EXISTS public.doctrine_migration_versions
(
    version character varying(191) COLLATE pg_catalog."default" NOT NULL,
    executed_at timestamp(0) without time zone DEFAULT NULL::timestamp without time zone,
    execution_time integer,
    CONSTRAINT doctrine_migration_versions_pkey PRIMARY KEY (version)
);

CREATE TABLE IF NOT EXISTS public.missions
(
    id integer NOT NULL,
    title character varying(255) COLLATE pg_catalog."default" NOT NULL,
    description text COLLATE pg_catalog."default" NOT NULL,
    code_name character varying(255) COLLATE pg_catalog."default" NOT NULL,
    start_date timestamp(0) without time zone NOT NULL,
    end_date timestamp(0) without time zone NOT NULL,
    status character varying(255) COLLATE pg_catalog."default" NOT NULL,
    type character varying(255) COLLATE pg_catalog."default" NOT NULL,
    specialities_id integer,
    country_id integer,
    CONSTRAINT missions_pkey PRIMARY KEY (id)
);

CREATE TABLE IF NOT EXISTS public.missions_agents
(
    missions_id integer NOT NULL,
    agents_id integer NOT NULL,
    CONSTRAINT missions_agents_pkey PRIMARY KEY (missions_id, agents_id)
);

CREATE TABLE IF NOT EXISTS public.missions_contacts
(
    missions_id integer NOT NULL,
    contacts_id integer NOT NULL,
    CONSTRAINT missions_contacts_pkey PRIMARY KEY (missions_id, contacts_id)
);

CREATE TABLE IF NOT EXISTS public.missions_stashs
(
    missions_id integer NOT NULL,
    stashs_id integer NOT NULL,
    CONSTRAINT missions_stashs_pkey PRIMARY KEY (missions_id, stashs_id)
);

CREATE TABLE IF NOT EXISTS public.missions_targets
(
    missions_id integer NOT NULL,
    targets_id integer NOT NULL,
    CONSTRAINT missions_targets_pkey PRIMARY KEY (missions_id, targets_id)
);

CREATE TABLE IF NOT EXISTS public.specialities
(
    id integer NOT NULL,
    name character varying(255) COLLATE pg_catalog."default" NOT NULL,
    CONSTRAINT specialities_pkey PRIMARY KEY (id)
);

CREATE TABLE IF NOT EXISTS public.stashs
(
    id integer NOT NULL,
    adress character varying(255) COLLATE pg_catalog."default" NOT NULL,
    postal_code integer NOT NULL,
    type character varying(255) COLLATE pg_catalog."default" NOT NULL,
    code character varying(255) COLLATE pg_catalog."default" NOT NULL,
    country_id integer,
    town character varying(255) COLLATE pg_catalog."default" DEFAULT NULL::character varying,
    CONSTRAINT stashs_pkey PRIMARY KEY (id)
);

CREATE TABLE IF NOT EXISTS public.targets
(
    id integer NOT NULL,
    last_name character varying(255) COLLATE pg_catalog."default" NOT NULL,
    first_name character varying(255) COLLATE pg_catalog."default" NOT NULL,
    birth_date date NOT NULL,
    code_name character varying(255) COLLATE pg_catalog."default" NOT NULL,
    country_id integer,
    CONSTRAINT targets_pkey PRIMARY KEY (id)
);

CREATE TABLE IF NOT EXISTS public."user"
(
    id integer NOT NULL,
    email character varying(180) COLLATE pg_catalog."default" NOT NULL,
    roles json NOT NULL,
    password character varying(255) COLLATE pg_catalog."default" NOT NULL,
    last_name character varying(255) COLLATE pg_catalog."default" NOT NULL,
    first_name character varying(255) COLLATE pg_catalog."default" NOT NULL,
    created_at timestamp(0) without time zone NOT NULL,
    CONSTRAINT user_pkey PRIMARY KEY (id)
);

COMMENT ON COLUMN public."user".created_at
    IS '(DC2Type:datetime_immutable)';

ALTER TABLE IF EXISTS public.agents
    ADD CONSTRAINT fk_9596ab6ef92f3e70 FOREIGN KEY (country_id)
    REFERENCES public.country (id) MATCH SIMPLE
    ON UPDATE NO ACTION
    ON DELETE NO ACTION;
CREATE INDEX IF NOT EXISTS idx_9596ab6ef92f3e70
    ON public.agents(country_id);


ALTER TABLE IF EXISTS public.agents_specialities
    ADD CONSTRAINT fk_4ea537fa709770dc FOREIGN KEY (agents_id)
    REFERENCES public.agents (id) MATCH SIMPLE
    ON UPDATE NO ACTION
    ON DELETE CASCADE;
CREATE INDEX IF NOT EXISTS idx_4ea537fa709770dc
    ON public.agents_specialities(agents_id);


ALTER TABLE IF EXISTS public.agents_specialities
    ADD CONSTRAINT fk_4ea537fa804698d6 FOREIGN KEY (specialities_id)
    REFERENCES public.specialities (id) MATCH SIMPLE
    ON UPDATE NO ACTION
    ON DELETE CASCADE;
CREATE INDEX IF NOT EXISTS idx_4ea537fa804698d6
    ON public.agents_specialities(specialities_id);


ALTER TABLE IF EXISTS public.contacts
    ADD CONSTRAINT fk_33401573f92f3e70 FOREIGN KEY (country_id)
    REFERENCES public.country (id) MATCH SIMPLE
    ON UPDATE NO ACTION
    ON DELETE NO ACTION;
CREATE INDEX IF NOT EXISTS idx_33401573f92f3e70
    ON public.contacts(country_id);


ALTER TABLE IF EXISTS public.missions
    ADD CONSTRAINT fk_34f1d47e804698d6 FOREIGN KEY (specialities_id)
    REFERENCES public.specialities (id) MATCH SIMPLE
    ON UPDATE NO ACTION
    ON DELETE NO ACTION;
CREATE INDEX IF NOT EXISTS idx_34f1d47e804698d6
    ON public.missions(specialities_id);


ALTER TABLE IF EXISTS public.missions
    ADD CONSTRAINT fk_34f1d47ef92f3e70 FOREIGN KEY (country_id)
    REFERENCES public.country (id) MATCH SIMPLE
    ON UPDATE NO ACTION
    ON DELETE NO ACTION;
CREATE INDEX IF NOT EXISTS idx_34f1d47ef92f3e70
    ON public.missions(country_id);


ALTER TABLE IF EXISTS public.missions_agents
    ADD CONSTRAINT fk_5340afb917c042cf FOREIGN KEY (missions_id)
    REFERENCES public.missions (id) MATCH SIMPLE
    ON UPDATE NO ACTION
    ON DELETE CASCADE;
CREATE INDEX IF NOT EXISTS idx_5340afb917c042cf
    ON public.missions_agents(missions_id);


ALTER TABLE IF EXISTS public.missions_agents
    ADD CONSTRAINT fk_5340afb9709770dc FOREIGN KEY (agents_id)
    REFERENCES public.agents (id) MATCH SIMPLE
    ON UPDATE NO ACTION
    ON DELETE CASCADE;
CREATE INDEX IF NOT EXISTS idx_5340afb9709770dc
    ON public.missions_agents(agents_id);


ALTER TABLE IF EXISTS public.missions_contacts
    ADD CONSTRAINT fk_fa54446417c042cf FOREIGN KEY (missions_id)
    REFERENCES public.missions (id) MATCH SIMPLE
    ON UPDATE NO ACTION
    ON DELETE CASCADE;
CREATE INDEX IF NOT EXISTS idx_fa54446417c042cf
    ON public.missions_contacts(missions_id);


ALTER TABLE IF EXISTS public.missions_contacts
    ADD CONSTRAINT fk_fa544464719fb48e FOREIGN KEY (contacts_id)
    REFERENCES public.contacts (id) MATCH SIMPLE
    ON UPDATE NO ACTION
    ON DELETE CASCADE;
CREATE INDEX IF NOT EXISTS idx_fa544464719fb48e
    ON public.missions_contacts(contacts_id);


ALTER TABLE IF EXISTS public.missions_stashs
    ADD CONSTRAINT fk_47f51b5217c042cf FOREIGN KEY (missions_id)
    REFERENCES public.missions (id) MATCH SIMPLE
    ON UPDATE NO ACTION
    ON DELETE CASCADE;
CREATE INDEX IF NOT EXISTS idx_47f51b5217c042cf
    ON public.missions_stashs(missions_id);


ALTER TABLE IF EXISTS public.missions_stashs
    ADD CONSTRAINT fk_47f51b52ee826336 FOREIGN KEY (stashs_id)
    REFERENCES public.stashs (id) MATCH SIMPLE
    ON UPDATE NO ACTION
    ON DELETE CASCADE;
CREATE INDEX IF NOT EXISTS idx_47f51b52ee826336
    ON public.missions_stashs(stashs_id);


ALTER TABLE IF EXISTS public.missions_targets
    ADD CONSTRAINT fk_b7328f6017c042cf FOREIGN KEY (missions_id)
    REFERENCES public.missions (id) MATCH SIMPLE
    ON UPDATE NO ACTION
    ON DELETE CASCADE;
CREATE INDEX IF NOT EXISTS idx_b7328f6017c042cf
    ON public.missions_targets(missions_id);


ALTER TABLE IF EXISTS public.missions_targets
    ADD CONSTRAINT fk_b7328f6043b5f743 FOREIGN KEY (targets_id)
    REFERENCES public.targets (id) MATCH SIMPLE
    ON UPDATE NO ACTION
    ON DELETE CASCADE;
CREATE INDEX IF NOT EXISTS idx_b7328f6043b5f743
    ON public.missions_targets(targets_id);


ALTER TABLE IF EXISTS public.stashs
    ADD CONSTRAINT fk_81231f85f92f3e70 FOREIGN KEY (country_id)
    REFERENCES public.country (id) MATCH SIMPLE
    ON UPDATE NO ACTION
    ON DELETE NO ACTION;
CREATE INDEX IF NOT EXISTS idx_81231f85f92f3e70
    ON public.stashs(country_id);


ALTER TABLE IF EXISTS public.targets
    ADD CONSTRAINT fk_af431e13f92f3e70 FOREIGN KEY (country_id)
    REFERENCES public.country (id) MATCH SIMPLE
    ON UPDATE NO ACTION
    ON DELETE NO ACTION;
CREATE INDEX IF NOT EXISTS idx_af431e13f92f3e70
    ON public.targets(country_id);

END;