<?php

namespace App\service;

use App\Entity\Missions;

/**
 * [Description ValidationMission] Validation de la demande de création ou d'édition d'une mission
 */
class ValidationMission
{
    public function errorsInMissionDemand(Missions $mission, array $errorsForm)
    {
        $nbr_contact = $this->contactVerification($mission);
        $nbr_stashs = $this->stashVerification($mission);
        $nbr_agent_with_speciality = $this->agentSpecialityVerification($mission);
        $nbr_agents_targets_same_nationality = $this->agentTargetVerification($mission);

        if ($nbr_agents_targets_same_nationality !== 0) {
            array_push($errorsForm, 'Les agents et les cibles ne peuvent avoir la même nationalité,
                                         veuillez changer d\'agents ou de cibles!');
        }
        if ($nbr_agent_with_speciality === 0) {
            array_push($errorsForm, "Aucun agent sélectionné ne possède la spécialité requise pour cette mission");
        }
        if ($nbr_contact > 0) {
            array_push($errorsForm, "Veuillez choisir des contacts ayant la nationalité du lieu me mission");
        }
        if ($nbr_stashs > 0) {
            array_push($errorsForm, "Veuillez choisir des planques présentent dans le pays de mission");
        }
        return $errorsForm;
    }

    private function agentSpecialityVerification($mission)
    {
        $nbr_agent_with_speciality = 0;
        foreach ($mission->getAgents() as $agent) {
            foreach ($agent->getSpecialities() as $agentSpeciality) {
                if ($agentSpeciality->getName() === $mission->getSpecialities()->getName()) {
                    $nbr_agent_with_speciality++;
                }
            }
        }
        return $nbr_agent_with_speciality;
    }
    private function agentTargetVerification($mission)
    {
        $nbr_agents_targets_same_nationality = 0;
        foreach ($mission->getAgents() as $agent) {
            foreach ($mission->getTargets() as $target) {
                if ($agent->getCountry()->getName() === $target->getCountry()->getName()) {
                    $nbr_agents_targets_same_nationality++;
                }
            }
        }
        return $nbr_agents_targets_same_nationality;
    }

    private function contactVerification($mission)
    {
        $nbr_contact = 0;
        foreach ($mission->getContacts() as $contact) {
            if ($contact->getCountry()->getName() !== $mission->getCountry()->getName()) {
                $nbr_contact++;
            }
        }
        return $nbr_contact;
    }

    private function stashVerification($mission)
    {
        $nbr_stashs = 0;
        foreach ($mission->getStashs() as $stash) {
            if ($stash->getCountry()->getName() !== $mission->getCountry()->getName()) {
                $nbr_stashs++;
            }
        }
        return $nbr_stashs;
    }
}
