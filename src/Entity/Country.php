<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use App\Repository\CountryRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ApiResource()
 * @ORM\Entity(repositoryClass=CountryRepository::class)
 */
class Country
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $name;

    /**
     * @ORM\Column(type="string", length=2)
     */
    private $iso_code;

    /**
     * @ORM\Column(type="string", length=7)
     */
    private $isd_code;

    /**
     * @ORM\OneToMany(targetEntity=Agents::class, mappedBy="country")
     */
    private $agent;

    /**
     * @ORM\OneToMany(targetEntity=Contacts::class, mappedBy="country")
     */
    private $contact;

    /**
     * @ORM\OneToMany(targetEntity=Stashs::class, mappedBy="country")
     */
    private $stash;

    /**
     * @ORM\OneToMany(targetEntity=Targets::class, mappedBy="country")
     */
    private $target;

    /**
     * @ORM\OneToMany(targetEntity=Missions::class, mappedBy="country")
     */
    private $mission;

    public function __construct()
    {
        $this->agent = new ArrayCollection();
        $this->contact = new ArrayCollection();
        $this->stash = new ArrayCollection();
        $this->target = new ArrayCollection();
        $this->mission = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getIsoCode(): ?string
    {
        return $this->iso_code;
    }

    public function setIsoCode(string $iso_code): self
    {
        $this->iso_code = $iso_code;

        return $this;
    }

    public function getIsdCode(): ?string
    {
        return $this->isd_code;
    }

    public function setIsdCode(string $isd_code): self
    {
        $this->isd_code = $isd_code;

        return $this;
    }

    /**
     * @return Collection|Agents[]
     */
    public function getAgent(): Collection
    {
        return $this->agent;
    }

    public function addAgent(Agents $agent): self
    {
        if (!$this->agent->contains($agent)) {
            $this->agent[] = $agent;
            $agent->setCountry($this);
        }

        return $this;
    }

    public function removeAgent(Agents $agent): self
    {
        if ($this->agent->removeElement($agent)) {
            // set the owning side to null (unless already changed)
            if ($agent->getCountry() === $this) {
                $agent->setCountry(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Contacts[]
     */
    public function getContact(): Collection
    {
        return $this->contact;
    }

    public function addContact(Contacts $contact): self
    {
        if (!$this->contact->contains($contact)) {
            $this->contact[] = $contact;
            $contact->setCountry($this);
        }

        return $this;
    }

    public function removeContact(Contacts $contact): self
    {
        if ($this->contact->removeElement($contact)) {
            // set the owning side to null (unless already changed)
            if ($contact->getCountry() === $this) {
                $contact->setCountry(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Stashs[]
     */
    public function getStash(): Collection
    {
        return $this->stash;
    }

    public function addStash(Stashs $stash): self
    {
        if (!$this->stash->contains($stash)) {
            $this->stash[] = $stash;
            $stash->setCountry($this);
        }

        return $this;
    }

    public function removeStash(Stashs $stash): self
    {
        if ($this->stash->removeElement($stash)) {
            // set the owning side to null (unless already changed)
            if ($stash->getCountry() === $this) {
                $stash->setCountry(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Targets[]
     */
    public function getTarget(): Collection
    {
        return $this->target;
    }

    public function addTarget(Targets $target): self
    {
        if (!$this->target->contains($target)) {
            $this->target[] = $target;
            $target->setCountry($this);
        }

        return $this;
    }

    public function removeTarget(Targets $target): self
    {
        if ($this->target->removeElement($target)) {
            // set the owning side to null (unless already changed)
            if ($target->getCountry() === $this) {
                $target->setCountry(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Missions[]
     */
    public function getMission(): Collection
    {
        return $this->mission;
    }

    public function addMission(Missions $mission): self
    {
        if (!$this->mission->contains($mission)) {
            $this->mission[] = $mission;
            $mission->setCountry($this);
        }

        return $this;
    }

    public function removeMission(Missions $mission): self
    {
        if ($this->mission->removeElement($mission)) {
            // set the owning side to null (unless already changed)
            if ($mission->getCountry() === $this) {
                $mission->setCountry(null);
            }
        }

        return $this;
    }

    public function __toString()
    {
        return $this->name;
    }
}
