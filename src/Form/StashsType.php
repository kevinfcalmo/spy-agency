<?php

namespace App\Form;

use App\Entity\Countries;
use App\Entity\Country;
use App\Entity\Stashs;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class StashsType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('adress', TextType::class, [
                'attr' => ['class' => 'form-control'],
                'label' => 'Adresse'
            ])
            ->add('postal_code', TextType::class, [
                'attr' => ['class' => 'form-control'],
                'label' => 'Code postal'
            ])
            ->add('type', ChoiceType::class, [
                'choices' => [
                    'Home' => 'home',
                    'Bumker' => 'bumker',
                    'Appartment' => 'appartment',
                    'Hut' => 'hut',
                    'Chalet' => 'chalet',
                ],
                'attr' => ['class' => 'form-select'],
                'label' => 'Type de planque'
            ])
            ->add('code', TextType::class, [
                'attr' => ['class' => 'form-control'],
                'label' => 'Code de la planque'
            ])
            ->add('town', TextType::class, [
                'attr' => ['class' => 'form-control'],
                'label' => 'Ville'
            ])
            ->add('country', EntityType::class, [
                'class' => Country::class,
                'choice_label' => 'name',
                'label' => 'Pays',
                'attr' => ['class' => 'form-select'],
            ]);
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Stashs::class,
        ]);
    }
}
