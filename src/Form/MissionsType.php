<?php

namespace App\Form;

use App\Entity\Agents;
use App\Entity\Contacts;
use App\Entity\Countries;
use App\Entity\Country;
use App\Entity\Missions;
use App\Entity\Specialities;
use App\Entity\Stashs;
use App\Entity\Targets;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class MissionsType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('title', TextType::class, [
                'attr' => ['class' => 'form-control'],
                'label' => 'Titre de la mission'
            ])
            ->add('description', TextType::class, [
                'attr' => [
                    'class' => 'form-control',
                    'rows' => '5',
                ],
                'label' => 'Description'
            ])
            ->add('code_name', TextType::class, [
                'attr' => ['class' => 'form-control'],
                'label' => 'Nom de code'
            ])
            ->add('start_date', DateType::class, [
                'label' => 'Date de début de mission'
            ])
            ->add('end_date', DateType::class, [
                'label' => 'Date de fin de mission'
            ])
            ->add('status', ChoiceType::class, [
                'choices' => [
                    'En preparation' => 'En preparation',
                    'En cours' => 'En cours',
                    'Terminé' => 'Terminé',
                    'Echec' => 'Echec'
                ],
                'attr' => ['class' => 'form-select'],
                'label' => 'Status de mission'
            ])
            ->add('type', ChoiceType::class, [
                'choices' => [
                    'Surveillance' => 'Surveillance',
                    'Assassinat' => 'Assassinat',
                    'Infiltration' => 'Infiltration',
                    'Elimination' => 'Elimination',
                ],
                'attr' => ['class' => 'form-select'],
                'label' => 'Type de mission'
            ])
            ->add('country', EntityType::class, [
                'class' => Country::class,
                'choice_label' => 'name',
                'label' => 'Pays',
                'attr' => ['class' => 'form-select'],
            ])
            ->add('specialities', EntityType::class, [
                'class' => Specialities::class,
                'choice_label' => 'name',
                'label' => 'Spécialité requise',
                'attr' => ['class' => 'form-select'],
            ])
            ->add('agents', EntityType::class, [
                'class' => Agents::class,
                'attr' => ['class' => 'form-select'],
                'multiple' => true,
                'label' => 'Liste des agents'
            ])
            ->add('targets', EntityType::class, [
                'class' => Targets::class,
                'attr' => ['class' => 'form-select'],
                'multiple' => true,
                'label' => 'Liste des cibles'
            ])
            ->add('contacts', EntityType::class, [
                'class' => Contacts::class,
                'attr' => ['class' => 'form-select'],
                'multiple' => true,
                'label' => 'Listes des contacts'
            ])
            ->add('stashs', EntityType::class, [
                'class' => Stashs::class,
                'attr' => ['class' => 'form-select'],
                'multiple' => true,
                'label' => 'Listes des planques'
            ]);
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Missions::class,
        ]);
    }
}
