<?php

namespace App\Form;

use App\Entity\Agents;
use App\Entity\Country;
use App\Entity\Specialities;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\BirthdayType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\TextType as TypeTextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class AgentsType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('last_name', TypeTextType::class, [
                'attr' => ['class' => 'form-control'],
                'label' => 'Nom'
            ])
            ->add('first_name', TypeTextType::class, [
                'attr' => ['class' => 'form-control'],
                'label' => 'Prénom'
            ])
            ->add('birth_date', BirthdayType::class, [
                'label' => 'Date de naissance'
            ])
            ->add('authentication_code', TypeTextType::class, [
                'attr' => ['class' => 'form-control'],
                'label' => 'Code d\'authentification'
            ])
            ->add('country', EntityType::class, [
                'class' => Country::class,
                'choice_label' => 'name',
                'label' => 'Nationalité',
                'attr' => ['class' => 'form-select'],
            ])
            ->add('specialities', EntityType::class, [
                'class' => Specialities::class,
                'choice_label' => 'name',
                'label' => 'Spécialités',
                'multiple' => true,
                'attr' => ['class' => 'form-select'],
            ]);
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Agents::class,
        ]);
    }
}
