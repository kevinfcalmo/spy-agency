<?php

namespace App\Controller;

use App\Entity\Contacts;
use App\Form\ContactsType;
use App\Repository\ContactsRepository;
use Doctrine\ORM\EntityManagerInterface;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Validator\Validator\ValidatorInterface;

/**
 * @Route("/admin", name="admin")
 */
class ContactsController extends AbstractController
{
    /**
     * @Route("/contacts", name="_contacts")
     */
    public function index(
        ContactsRepository $contactsRepository,
        Request $request,
        PaginatorInterface $paginatorInterface
    ): Response {

        $donnees = $contactsRepository->findAll();

        $contacts = $paginatorInterface->paginate(
            $donnees,
            $request->query->getInt('page', 1),
            5
        );
        return $this->render('contacts/index.html.twig', [
            'controller_name' => 'ContactsController',
            'items' => $contacts,
            'type' => 'contact',
            'title' => 'contacts',
        ]);
    }
    /**
     * @Route("/add-contact" , name="_add-contact")
     */
    public function addContact(
        Request $request,
        ValidatorInterface $validatorInterface
    ): Response {
        $contact = new Contacts();
        $form = $this->createForm(ContactsType::class, $contact);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getdoctrine()->getManager();
            $entityManager->persist($contact);
            $entityManager->flush();
            return $this->redirectToRoute("admin_contacts");
        }
        return $this->render('contacts/form.html.twig', [
            'form' => $form->createView(),
            'type' => 'contact',
            'function' => 'Creer'
        ]);
    }

    /**
     * @Route("/edit-contact/{id}" , name="_edit-contact")
     */
    public function editContact(
        $id,
        Request $request,
        ContactsRepository $contactsRepository
    ): Response {
        $contact = $contactsRepository->find($id);
        $form = $this->createForm(ContactsType::class, $contact);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getdoctrine()->getManager();
            $entityManager->flush();
            return $this->redirectToRoute("admin_contacts");
        }
        return $this->render('contacts/form.html.twig', [
            'form' => $form->createView(),
            'type' => 'contact',
            'function' => 'Editer'
        ]);
    }
    /**
     * @Route("/remove-contact/{id}" , name="_remove-contact")
     */
    public function removeContact($id, ContactsRepository $contactsRepository, EntityManagerInterface $entityManager)
    {
        $contact = $contactsRepository->find($id);
        $entityManager->remove($contact);
        $entityManager->flush();
        return $this->redirectToRoute("admin_contacts");
    }
}
