<?php

namespace App\Controller;

use ApiPlatform\Core\DataProvider\PaginatorInterface;
use App\Entity\User;
use App\Form\RegistrationFormType;
use App\Form\UserType;
use App\Repository\UserRepository;
use App\Security\AdministratorAuthenticator;
use DateTimeImmutable;
use Doctrine\ORM\EntityManagerInterface;
use Knp\Component\Pager\PaginatorInterface as PagerPaginatorInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Http\Authentication\UserAuthenticatorInterface;

/**
 * @Route("/admin" , name="admin")
 */
class UsersController extends AbstractController
{
    /**
     * @Route("/users" , name ="_users")
     */
    public function home(
        UserRepository $userRepository,
        Request $request,
        PagerPaginatorInterface $paginatorInterface
    ): Response {
        $donnees = $userRepository->findAll();
        $users = $paginatorInterface->paginate(
            $donnees,
            $request->query->getInt('page', 1),
            10
        );

        return $this->render('registration/index.html.twig', [
            'items' => $users,
            'type' => 'user',
            'title' => 'users',
        ]);
    }

    /**
     * @Route("/add-user", name="_add-user")
     */
    public function register(
        Request $request,
        UserPasswordHasherInterface $userPasswordHasher,
        UserAuthenticatorInterface $userAuthenticator,
        AdministratorAuthenticator $authenticator,
        EntityManagerInterface $entityManager
    ): Response {
        $user = new User();
        $form = $this->createForm(RegistrationFormType::class, $user);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            // encode the plain password
            $user->setPassword(
                $userPasswordHasher->hashPassword(
                    $user,
                    $form->get('plainPassword')->getData()
                )
            );
            $user->setCreatedAt(new DateTimeImmutable());

            $entityManager->persist($user);
            $entityManager->flush();
            // do anything else you need here, like send an email

            return $this->redirectToRoute('admin_users');
        }

        return $this->render('registration/register.html.twig', [
            'registrationForm' => $form->createView(),
            'title' => "Création d'un nouvel utilisateur",
        ]);
    }

    /**
     * @Route("/edit-user/{id}", name="_app_register")
     */
    public function editUser(
        int $id,
        UserRepository $userRepository,
        Request $request,
        UserPasswordHasherInterface $userPasswordHasher,
        UserAuthenticatorInterface $userAuthenticator,
        AdministratorAuthenticator $authenticator,
        EntityManagerInterface $entityManager
    ): Response {
        $user = $userRepository->find($id);
        $form = $this->createForm(UserType::class, $user);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager->persist($user);
            $entityManager->flush();
            // do anything else you need here, like send an email

            return $userAuthenticator->authenticateUser(
                $user,
                $authenticator,
                $request
            );
        }

        return $this->render('registration/edit-user.html.twig', [
            'registrationForm' => $form->createView(),
            'title' => "Edition d'un utilisateur",
        ]);
    }

    /**
     * @Route("/remove-user/{id}" , name="_remove-user")
     */
    public function removeAgent($id, UserRepository $userRepository, EntityManagerInterface $entityManager)
    {
        $user =  $userRepository->find($id);
        $entityManager->remove($user);
        $entityManager->flush();
        return $this->redirectToRoute("admin_users");
    }
}
