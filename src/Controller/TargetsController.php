<?php

namespace App\Controller;

use App\Entity\Targets;
use App\Form\TargetsType;
use App\Repository\TargetsRepository;
use Doctrine\ORM\EntityManagerInterface;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/admin", name="admin")
 */
class TargetsController extends AbstractController
{
    /**
     * @Route("/targets", name="_targets")
     */
    public function index(
        TargetsRepository $targetsRepository,
        Request $request,
        PaginatorInterface $paginatorInterface
    ): Response {

        $donnees = $targetsRepository->findAll();

        $targets = $paginatorInterface->paginate(
            $donnees,
            $request->query->getInt('page', 1),
            5
        );


        return $this->render('targets/index.html.twig', [
            'items' => $targets,
            'type' => 'target',
            'title' => 'cibles',
        ]);
    }

    /**
     * @Route("/add-target" , name="_add-target")
     */
    public function addAgent(Request $request): Response
    {
        $target = new Targets();
        $form = $this->createForm(TargetsType::class, $target);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getdoctrine()->getManager();
            $entityManager->persist($target);
            $entityManager->flush();
            return $this->redirectToRoute("admin_targets");
        }




        return $this->render('contacts/form.html.twig', [
            'form' => $form->createView(),
            'type' => 'target',
            'function' => 'Creer'
        ]);
    }

    /**
     * @Route("/edit-target/{id}" , name="_edit-target")
     */
    public function editTarget($id, Request $request, TargetsRepository $targetsRepository): Response
    {
        $target = $targetsRepository->find($id);
        $form = $this->createForm(TargetsType::class, $target);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getdoctrine()->getManager();
            $entityManager->flush();
            return $this->redirectToRoute("admin_targets");
        }


        return $this->render('contacts/form.html.twig', [
            'form' => $form->createView(),
            'type' => 'target',
            'function' => 'Editer'
        ]);
    }

    /**
     * @Route("/remove-target/{id}" , name="_remove-target")
     */
    public function removeTarget($id, TargetsRepository $targetsRepository, EntityManagerInterface $entityManager)
    {
        $target = $targetsRepository->find($id);
        $entityManager->remove($target);
        $entityManager->flush();
        return $this->redirectToRoute("admin_targets");
    }
}
