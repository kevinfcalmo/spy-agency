<?php

namespace App\Controller;

use App\Entity\Specialities;
use App\Form\SpecialitiesType;
use App\Repository\SpecialitiesRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class SpecialitiesController extends AbstractController
{
    /**
     * @Route("/specialities", name="specialities")
     */
    public function index(): Response
    {
        return $this->render('specialities/index.html.twig', [
            'controller_name' => 'SpecialitiesController',
        ]);
    }

    /**
     * @Route("/speciality/new", name="new-speciality")
     */
    public function newSpeciality(
        Request $request,
        EntityManagerInterface $entityManager
    ): Response {

        $speciality = new Specialities();
        $form = $this->createForm(SpecialitiesType::class, $speciality);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager->persist($speciality);
            $entityManager->flush();
            return $this->redirectToRoute("specialities");
        }

        return $this->render('form-test.html.twig', [
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/speciality/edit/{id}", name="edit-speciality")
     */
    public function editSpeciality(
        $id,
        SpecialitiesRepository $specialitiesRepository,
        Request $request,
        EntityManagerInterface $entityManager
    ): Response {

        $speciality = $specialitiesRepository->find($id);
        $form = $this->createForm(SpecialitiesType::class, $speciality);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager->flush();
            return $this->redirectToRoute("specialities");
        }

        return $this->render('form-test.html.twig', [
            'form' => $form->createView(),
        ]);
    }
}
