<?php

namespace App\Controller;

use App\Entity\Stashs;
use App\Form\StashsType;
use App\Repository\StashsRepository;
use Doctrine\ORM\EntityManagerInterface;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/admin" , name="admin")
 */
class StashsController extends AbstractController
{
    /**
     * @Route("/stashs", name="_stashs")
     */
    public function index(
        StashsRepository $stashsRepository,
        Request $request,
        PaginatorInterface $paginatorInterface
    ): Response {
        $donnees = $stashsRepository->findAll();

        $stashs = $paginatorInterface->paginate(
            $donnees,
            $request->query->getInt('page', 1),
            5
        );

        return $this->render('stashs/index.html.twig', [
            'items' => $stashs,
            'type' => 'stash',
            'title' => 'planques',
        ]);
    }

    /**
     * @Route("/add-stash" , name="_add-stash")
     */
    public function addAgent(Request $request): Response
    {
        $stash = new Stashs();
        $form = $this->createForm(StashsType::class, $stash);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getdoctrine()->getManager();
            $entityManager->persist($stash);
            $entityManager->flush();
            return $this->redirectToRoute("admin_stashs");
        }




        return $this->render('stashs/form.html.twig', [
            'form' => $form->createView(),
            'type' => 'stash',
            'function' => 'Creer'
        ]);
    }

    /**
     * @Route("/edit-stash/{id}" , name="_edit-stash")
     */
    public function editAgent($id, Request $request, StashsRepository $stashsRepository): Response
    {
        $stash = $stashsRepository->find($id);
        $form = $this->createForm(StashsType::class, $stash);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getdoctrine()->getManager();
            $entityManager->flush();
            return $this->redirectToRoute("admin_stashs");
        }


        return $this->render('stashs/form.html.twig', [
            'form' => $form->createView(),
            'type' => 'stash',
            'function' => 'Editer'
        ]);
    }

    /**
     * @Route("/remove-stash/{id}" , name="_remove-stash")
     */
    public function removeStash($id, StashsRepository $stashsRepository, EntityManagerInterface $entityManager)
    {
        $stash = $stashsRepository->find($id);
        $entityManager->remove($stash);
        $entityManager->flush();
        return $this->redirectToRoute("admin_stashs");
    }
}
