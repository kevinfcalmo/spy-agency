<?php

namespace App\Controller;

use App\Entity\Agents;
use App\Form\AgentsType;
use App\Repository\AgentsRepository;
use Doctrine\ORM\EntityManagerInterface;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/admin" , name="admin")
 */
class AgentsController extends AbstractController
{
    /**
     * @Route("/agents", name="_agents")
     */
    public function index(
        AgentsRepository $agentsRepository,
        Request $request,
        PaginatorInterface $paginatorInterface
    ): Response {
        $donnees = $agentsRepository->findAll();
        $agents = $paginatorInterface->paginate(
            $donnees,
            $request->query->getInt('page', 1),
            5
        );

        return $this->render('agents/index.html.twig', [
            'items' => $agents,
            'type' => 'agent',
            'title' => 'agents',
        ]);
    }
    /**
     * @Route("/add-agent" , name="_add-agent")
     */
    public function addAgent(Request $request): Response
    {
        $agent = new Agents();
        $form = $this->createForm(AgentsType::class, $agent);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getdoctrine()->getManager();
            $entityManager->persist($agent);
            $entityManager->flush();
            return $this->redirectToRoute("admin_agents");
        }




        return $this->render('agents/form.html.twig', [
            'form' => $form->createView(),
            'type' => 'agent',
            'function' => 'Creer'
        ]);
    }


    /**
     * @Route("/edit-agent/{id}" , name="_edit-agent")
     */
    public function editAgent($id, Request $request, AgentsRepository $agentsRepository): Response
    {
        $agent = $agentsRepository->find($id);
        $form = $this->createForm(AgentsType::class, $agent);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getdoctrine()->getManager();
            $entityManager->flush();
            return $this->redirectToRoute("admin_agents");
        }


        return $this->render('agents/form.html.twig', [
            'form' => $form->createView(),
            'type' => 'agent',
            'function' => 'Editer'
        ]);
    }

    /**
     * @Route("/remove-agent/{id}" , name="_remove-agent")
     */
    public function removeAgent($id, AgentsRepository $agentsRepository, EntityManagerInterface $entityManager)
    {
        $agent = $agentsRepository->find($id);
        $entityManager->remove($agent);
        $entityManager->flush();
        return $this->redirectToRoute("admin_agents");
    }
}
