<?php

namespace App\Controller;

use App\Repository\AgentsRepository;
use App\Repository\ContactsRepository;
use App\Repository\MissionsRepository;
use App\Repository\StashsRepository;
use App\Repository\TargetsRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class AdminController extends AbstractController
{
    /**
     * @Route("/admin", name="admin")
     */
    public function index(
        AgentsRepository $agentsRepository,
        MissionsRepository $missionsRepository,
        ContactsRepository $contactsRepository,
        StashsRepository $stashsRepository,
        TargetsRepository $targetsRepository
    ): Response {

        $agents = $agentsRepository->findAll();
        $missions = $missionsRepository->findAll();

        $contacts = $contactsRepository->findAll();
        $stashs = $stashsRepository->findAll();
        $targets = $targetsRepository->findAll();

        return $this->render('admin/index.html.twig', [
            'agents' => $agents,
            'missions' => $missions,
            'contacts' => $contacts,
            'stashs' => $stashs,
            'targets' => $targets,
        ]);
    }
}
