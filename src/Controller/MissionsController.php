<?php

namespace App\Controller;

use App\Entity\Missions;
use App\Form\MissionsType;
use App\Repository\MissionsRepository;
use App\service\ValidationMission;
use Doctrine\ORM\EntityManagerInterface;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/admin" , name="admin")
 */
class MissionsController extends AbstractController
{
    /**
     * @Route("/missions", name="_missions")
     */
    public function index(
        MissionsRepository $missionsRepository,
        Request $request,
        PaginatorInterface $paginatorInterface
    ): Response {

        $donnees = $missionsRepository->findAll();

        $missions = $paginatorInterface->paginate(
            $donnees,
            $request->query->getInt('page', 1),
            10
        );


        return $this->render('missions/index.html.twig', [
            'items' => $missions,
            'type' => 'mission',
            'title' => 'missions',
        ]);
    }

    /**
     * @Route("/add-mission" , name="_add-mission")
     */
    public function newMission(
        Request $request,
        EntityManagerInterface $entityManager,
        ValidationMission $validationMission
    ): Response {

        $mission = new Missions();
        $form = $this->createForm(MissionsType::class, $mission);
        $form->handleRequest($request);
        $errorsForm = [];
        $errors = [];
        if ($form->isSubmitted() && $form->isValid()) {
            $errors = $validationMission->errorsInMissionDemand($mission, $errorsForm);
            if (empty($errors)) {
                $entityManager->persist($mission);
                $entityManager->flush();
                return $this->redirectToRoute("admin_missions");
            }
        }

        return $this->render('missions/form.html.twig', [
            'form' => $form->createView(),
            'type' => 'mission',
            'function' => 'Creer',
            'errors' => $errors
        ]);
    }


    /**
     * @Route("/edit-mission/{id}" , name="edit-mission")
     */
    public function editMission(
        $id,
        MissionsRepository $missionsRepository,
        Request $request,
        EntityManagerInterface $entityManager,
        ValidationMission $validationMission
    ): Response {

        $mission = $missionsRepository->find($id);
        $form = $this->createForm(MissionsType::class, $mission);
        $form->handleRequest($request);
        $errorsForm = [];
        $errors = [];
        if ($form->isSubmitted() && $form->isValid()) {
            $errors = $validationMission->errorsInMissionDemand($mission, $errorsForm);
            if (empty($errorsForm)) {
                $entityManager->flush();
                return $this->redirectToRoute("admin_missions");
            }
        }

        return $this->render('missions/form.html.twig', [
            'form' => $form->createView(),
            'type' => 'mission',
            'function' => 'Creer',
            'errors' => $errors,
        ]);
    }
    /**
     * @Route("/remove-mission/{id}" , name="_remove-mission")
     */
    public function removeMission($id, MissionsRepository $missionsRepository, EntityManagerInterface $entityManager)
    {
        $mission = $missionsRepository->find($id);
        $entityManager->remove($mission);
        $entityManager->flush();
        return $this->redirectToRoute("admin_missions");
    }
}
