<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20220121112730 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE contacts ADD country_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE contacts ADD CONSTRAINT FK_33401573F92F3E70 FOREIGN KEY (country_id) REFERENCES country (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('CREATE INDEX IDX_33401573F92F3E70 ON contacts (country_id)');
        $this->addSql('ALTER TABLE missions ADD country_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE missions ADD CONSTRAINT FK_34F1D47EF92F3E70 FOREIGN KEY (country_id) REFERENCES country (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('CREATE INDEX IDX_34F1D47EF92F3E70 ON missions (country_id)');
        $this->addSql('ALTER TABLE stashs ADD country_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE stashs ADD CONSTRAINT FK_81231F85F92F3E70 FOREIGN KEY (country_id) REFERENCES country (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('CREATE INDEX IDX_81231F85F92F3E70 ON stashs (country_id)');
        $this->addSql('ALTER TABLE targets ADD country_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE targets ADD CONSTRAINT FK_AF431E13F92F3E70 FOREIGN KEY (country_id) REFERENCES country (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('CREATE INDEX IDX_AF431E13F92F3E70 ON targets (country_id)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE SCHEMA public');
        $this->addSql('ALTER TABLE contacts DROP CONSTRAINT FK_33401573F92F3E70');
        $this->addSql('DROP INDEX IDX_33401573F92F3E70');
        $this->addSql('ALTER TABLE contacts DROP country_id');
        $this->addSql('ALTER TABLE targets DROP CONSTRAINT FK_AF431E13F92F3E70');
        $this->addSql('DROP INDEX IDX_AF431E13F92F3E70');
        $this->addSql('ALTER TABLE targets DROP country_id');
        $this->addSql('ALTER TABLE stashs DROP CONSTRAINT FK_81231F85F92F3E70');
        $this->addSql('DROP INDEX IDX_81231F85F92F3E70');
        $this->addSql('ALTER TABLE stashs DROP country_id');
        $this->addSql('ALTER TABLE missions DROP CONSTRAINT FK_34F1D47EF92F3E70');
        $this->addSql('DROP INDEX IDX_34F1D47EF92F3E70');
        $this->addSql('ALTER TABLE missions DROP country_id');
    }
}
