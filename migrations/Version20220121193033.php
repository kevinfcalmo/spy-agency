<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20220121193033 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE agents_specialities (agents_id INT NOT NULL, specialities_id INT NOT NULL, PRIMARY KEY(agents_id, specialities_id))');
        $this->addSql('CREATE INDEX IDX_4EA537FA709770DC ON agents_specialities (agents_id)');
        $this->addSql('CREATE INDEX IDX_4EA537FA804698D6 ON agents_specialities (specialities_id)');
        $this->addSql('ALTER TABLE agents_specialities ADD CONSTRAINT FK_4EA537FA709770DC FOREIGN KEY (agents_id) REFERENCES agents (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE agents_specialities ADD CONSTRAINT FK_4EA537FA804698D6 FOREIGN KEY (specialities_id) REFERENCES specialities (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE SCHEMA public');
        $this->addSql('DROP TABLE agents_specialities');
    }
}
