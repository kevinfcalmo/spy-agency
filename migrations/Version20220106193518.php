<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20220106193518 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE specialities_agents (specialities_id INT NOT NULL, agents_id INT NOT NULL, PRIMARY KEY(specialities_id, agents_id))');
        $this->addSql('CREATE INDEX IDX_FDFD3E60804698D6 ON specialities_agents (specialities_id)');
        $this->addSql('CREATE INDEX IDX_FDFD3E60709770DC ON specialities_agents (agents_id)');
        $this->addSql('ALTER TABLE specialities_agents ADD CONSTRAINT FK_FDFD3E60804698D6 FOREIGN KEY (specialities_id) REFERENCES specialities (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE specialities_agents ADD CONSTRAINT FK_FDFD3E60709770DC FOREIGN KEY (agents_id) REFERENCES agents (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE missions ADD specialities_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE missions ADD CONSTRAINT FK_34F1D47E804698D6 FOREIGN KEY (specialities_id) REFERENCES specialities (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('CREATE INDEX IDX_34F1D47E804698D6 ON missions (specialities_id)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE SCHEMA public');
        $this->addSql('DROP TABLE specialities_agents');
        $this->addSql('ALTER TABLE missions DROP CONSTRAINT FK_34F1D47E804698D6');
        $this->addSql('DROP INDEX IDX_34F1D47E804698D6');
        $this->addSql('ALTER TABLE missions DROP specialities_id');
    }
}
