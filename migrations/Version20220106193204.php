<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20220106193204 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE missions ADD countries_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE missions ADD CONSTRAINT FK_34F1D47EAEBAE514 FOREIGN KEY (countries_id) REFERENCES countries (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('CREATE INDEX IDX_34F1D47EAEBAE514 ON missions (countries_id)');
        $this->addSql('ALTER TABLE stashs ADD countries_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE stashs ADD CONSTRAINT FK_81231F85AEBAE514 FOREIGN KEY (countries_id) REFERENCES countries (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('CREATE INDEX IDX_81231F85AEBAE514 ON stashs (countries_id)');
        $this->addSql('ALTER TABLE targets ADD countries_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE targets ADD CONSTRAINT FK_AF431E13AEBAE514 FOREIGN KEY (countries_id) REFERENCES countries (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('CREATE INDEX IDX_AF431E13AEBAE514 ON targets (countries_id)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE SCHEMA public');
        $this->addSql('ALTER TABLE targets DROP CONSTRAINT FK_AF431E13AEBAE514');
        $this->addSql('DROP INDEX IDX_AF431E13AEBAE514');
        $this->addSql('ALTER TABLE targets DROP countries_id');
        $this->addSql('ALTER TABLE stashs DROP CONSTRAINT FK_81231F85AEBAE514');
        $this->addSql('DROP INDEX IDX_81231F85AEBAE514');
        $this->addSql('ALTER TABLE stashs DROP countries_id');
        $this->addSql('ALTER TABLE missions DROP CONSTRAINT FK_34F1D47EAEBAE514');
        $this->addSql('DROP INDEX IDX_34F1D47EAEBAE514');
        $this->addSql('ALTER TABLE missions DROP countries_id');
    }
}
